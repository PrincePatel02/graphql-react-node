const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const demoSchema = new Schema(
  {
    backIdAttempts: String,
    countryCode: String,
    deviceType: String,
    displayName: String,
    documentFrontSubtype: String,
    externalId: String,
    faceBrightness: String,
    faceRecognitionScoreStatus: String,
    faceValidationFinished: String,
    finishStatus: String,
    frontIdAttempts: String,
    gender: String,
    idOcrValidationScore: String,
    incodeIdValidationScoreStatus: String,
    issuingCountry: String,
    nationality: String,
    totalScoreStatus: String,
    typeOfId: String,
  },
  { collection: "demo" }
);

module.exports = mongoose.model("Demo", demoSchema);
