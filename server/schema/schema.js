const graphql = require("graphql");
const _ = require("lodash");
const Book = require("../models/book");
const Author = require("../models/author");
const Demo = require("../models/demo");

const {
  GraphQLObjectType,
  GraphQLString,
  GraphQLSchema,
  GraphQLID,
  GraphQLInt,
  GraphQLList,
  GraphQLNonNull,
} = graphql;

//Define BookType for How our Graph will look.
const BookType = new GraphQLObjectType({
  name: "Book",
  fields: () => ({
    id: { type: GraphQLString },
    name: { type: GraphQLString },
    genre: { type: GraphQLString },
    author: {
      type: AutherType,
      resolve(parent, args) {
        console.log(parent);
        // return _.find(authors, { id: parent.authorId });
        return Author.findById(parent.authorId);
      },
    },
  }),
});

// const DemoType = new GraphQLObjectType({
//   name: "Demo",
//   fields: () => ({
//     backIdAttempts: { type: GraphQLString },
//     countryCode: { type: GraphQLString },
//     deviceType: { type: GraphQLString },
//     displayName: { type: GraphQLString },
//     documentFrontSubtype: { type: GraphQLString },
//     externalId: { type: GraphQLString },
//     faceBrightness: { type: GraphQLString },
//     faceRecognitionScoreStatus: { type: GraphQLString },
//     faceValidationFinished: { type: GraphQLString },
//     finishStatus: { type: GraphQLString },
//     frontIdAttempts: { type: GraphQLString },
//     gender: { type: GraphQLString },
//     idOcrValidationScore: { type: GraphQLString },
//     incodeIdValidationScoreStatus: { type: GraphQLString },
//     issuingCountry: { type: GraphQLString },
//     nationality: { type: GraphQLString },
//     totalScoreStatus: { type: GraphQLString },
//     typeOfId: { type: GraphQLString },
//   }),
// });

const AutherType = new GraphQLObjectType({
  name: "Auther",
  fields: () => ({
    id: { type: GraphQLString },
    name: { type: GraphQLString },
    age: { type: GraphQLInt },
    books: {
      type: new GraphQLList(BookType),
      resolve(parent, args) {
        // return _.filter(books, { authorId: parent.id });
        return Book.find({
          authorId: parent.id,
        });
      },
    },
  }),
});

const RootQuery = new GraphQLObjectType({
  name: "RootQueryType",
  fields: {
    book: {
      type: BookType,
      args: { id: { type: GraphQLID } },
      resolve(parent, args) {
        //code to get data from db / other source
        //return _.find(books, { id: args.id });
        return Book.findById(args.id);
      },
    },
    author: {
      type: AutherType,
      args: { id: { type: GraphQLID } },
      resolve(parent, args) {
        // return _.find(authors, { id: args.id });
        return Author.findById(args.id);
      },
    },
    books: {
      type: new GraphQLList(BookType),
      resolve(parent, args) {
        // return books;
        return Book.find({});
      },
    },
    authors: {
      type: new GraphQLList(AutherType),
      resolve(parent, args) {
        // return authors;
        return Author.find({});
      },
    },
    // demos: {
    //   type: new GraphQLList(DemoType),
    //   resolve(parent, args) {
    //     // return books;
    //     return Demo.find({});
    //   },
    // },
  },
});

const Mutation = new GraphQLObjectType({
  name: "Mutation",
  fields: {
    addAuthor: {
      type: AutherType,
      args: {
        name: { type: new GraphQLNonNull(GraphQLString) },
        age: { type: new GraphQLNonNull(GraphQLInt) },
      },
      resolve(parent, args) {
        let author = new Author({
          name: args.name,
          age: args.age,
        });
        return author.save();
      },
    },
    addBook: {
      type: BookType,
      args: {
        name: { type: new GraphQLNonNull(GraphQLString) },
        genre: { type: new GraphQLNonNull(GraphQLString) },
        authorId: { type: new GraphQLNonNull(GraphQLID) },
      },
      resolve(parent, args) {
        let book = new Book({
          name: args.name,
          genre: args.genre,
          authorId: args.authorId,
        });
        return book.save();
      },
    },
  },
});

module.exports = new GraphQLSchema({ query: RootQuery, mutation: Mutation });
