const express = require("express");
const { graphqlHTTP } = require("express-graphql");
const schema = require("./schema/schema");
const mongoose = require("mongoose");
const cors = require("cors");
const app = express();

app.use(cors());

app.use(
  "/graphql",
  graphqlHTTP({
    schema,
    graphiql: true,
  })
);



mongoose.connect(
  "mongodb+srv://prince:7572846141@graphqlcluster.wkafg8o.mongodb.net/?retryWrites=true&w=majority"
);

mongoose.connection.once("open", () => {
  console.log("Connected to Database");
});

app.listen(4000, () => {
  console.log("listening on port 4000");
});
